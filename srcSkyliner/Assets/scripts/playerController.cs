﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour {

	// GAME MANAGER
	GameManager GM;
	void Awake () {
		GM = GameManager.Instancia;
	}


	// att
	Rigidbody2D naveRB;
	Animator naveAnim;
	public GameObject disparoGO;
	Rigidbody2D disparoRB;
	private float disparoProximo;

	// audio
	public AudioClip sonidoDisparo, sonidoRecibirDano; 


	void Start () {

		// inicializacion de componentes
		naveRB = GetComponent<Rigidbody2D>();
		naveAnim = GetComponent<Animator>();

	}
	

	void Update () {

		if (Input.GetKey(KeyCode.Space) && Time.time > disparoProximo)
		{
			disparoProximo = Time.time + GM.minave.disparoRate;
			GM.ejecutarSonido(sonidoDisparo);

			switch(GM.minave.disparoAncho){
			case 1:
				Instantiate(disparoGO, transform.position + new Vector3(0,0.5f,0), Quaternion.identity);
				break;
			
			case 2:
				Instantiate(disparoGO, transform.position + new Vector3(0.2f,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(-0.2f,0.5f,0), Quaternion.identity);
				break;

			case 3:
				Instantiate(disparoGO, transform.position + new Vector3(0,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(0.3f,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(-0.3f,0.5f,0), Quaternion.identity);
				break;

			case 4:
				Instantiate(disparoGO, transform.position + new Vector3(0.2f,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(-0.2f,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(0.4f,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(-0.4f,0.5f,0), Quaternion.identity);
				break;

			default:
				Instantiate(disparoGO, transform.position + new Vector3(0,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(0.3f,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(-0.3f,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(0.5f,0.5f,0), Quaternion.identity);
				Instantiate(disparoGO, transform.position + new Vector3(-0.5f,0.5f,0), Quaternion.identity);
				break;
			}


		}
	
	}



	void FixedUpdate () {

		// captura
		float h = Input.GetAxis ("Horizontal");
		float v = Input.GetAxis ("Vertical");

		// movimiento
		naveRB.velocity = new Vector2 (h, v) * GM.minave.naveVel;

		// para evitar que salga de la pantalla
		naveRB.position = new Vector2 (
			Mathf.Clamp (naveRB.position.x, GM.boundary.xMin, GM.boundary.xMax), 
			Mathf.Clamp (naveRB.position.y, GM.boundary.yMin, GM.boundary.yMax)
			);

		// animaciones
		if(h<0) naveAnim.SetTrigger("izquierda");
		if(h>0) naveAnim.SetTrigger("derecha");

	}


	// Colisiones
	void OnCollisionEnter2D(Collision2D c) {

		// Enemigo
		if (c.gameObject.CompareTag("Enemy")){
			GM.minave.hacerDano();
			GM.puntos++;
			GM.ejecutarSonido(sonidoRecibirDano);
			Destroy(c.gameObject);
		}
	

	}



}
