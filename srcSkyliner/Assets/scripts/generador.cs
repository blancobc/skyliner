﻿using UnityEngine;
using System.Collections;

public class generador : MonoBehaviour {

	// GAME MANAGER
	GameManager GM;
	void Awake () {
		GM = GameManager.Instancia;
	}

	
	public GameObject enemigo;
	

	void Start () {

		// inicio la nave
		GM.minave.naveVida = 100;
		GM.minave.reiniciarCaracteristicas();
	
		// generador de oleadas
		InvokeRepeating ("generaEnemigo",1,1);

	}


	// genero enemigos por encima de la pantalla en un x aleatorio dentro de los limites
	public void generaEnemigo(){
		Vector2 v = new Vector2( Random.Range(GM.boundary.xMin + 0.2f, GM.boundary.xMax - 0.2f), 
		                        GM.boundary.yMax + 1);
		Instantiate(enemigo, v, Quaternion.identity);
	}
	

	void Update () {

	}
}
