using UnityEngine;

public class Intro : MonoBehaviour {

	// GAME MANAGER
	GameManager GM;
	void Awake () {
		GM = GameManager.Instancia;
	}
	
	// Al comenzar cambiamos al menu principal después de una espera
	void Start () {
		GM.estadoActual = Estado.MENU;
		Invoke("CargaEscena", 2f);
	}

	public void CargaEscena(){
		GM.irAlMenu();
	}


}
