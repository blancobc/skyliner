﻿using UnityEngine;
using System.Collections;

public enum TipoPickups { VIDA, VELOCIDAD, DISPARO }

public class pickupController : MonoBehaviour {


	// GAME MANAGER
	GameManager GM;
	void Awake () {
		GM = GameManager.Instancia;
	}

	// componentes
	SpriteRenderer sr;

	// atributos
	public TipoPickups tipo {get; private set;}
	private float enemigoVelocidad;
	public AudioClip sonidoPickup;


	void Start () {
	
		// enlace a componentes
		enemigoVelocidad = GM.minave.enemigoVelocidad;
		sr = GetComponent<SpriteRenderer>();

		// inicialmente le ponemos un tipo
		this.tipo = TipoPickups.DISPARO;
		PintaPickup();
		InvokeRepeating("CambiaTipo", 2, 2);
	}


	void FixedUpdate () {

		transform.Translate(new Vector3(0, -enemigoVelocidad * Time.deltaTime, 0));

	}


	// matamos los que salen de la pantalla
	void OnBecameInvisible() { Destroy( gameObject ); }

	// manejo de tipos de pickups
	void CambiaTipo(){
		tipo++;
		if(tipo > TipoPickups.DISPARO) tipo = 0;
		PintaPickup();
	}
	void PintaPickup(){
		switch(tipo){
		case TipoPickups.DISPARO:
			sr.color = Color.red;
			break;
		case TipoPickups.VELOCIDAD:
			sr.color = Color.yellow;
			break;
		case TipoPickups.VIDA:
			sr.color = Color.green;
			break;
		}
	}

	// Colisiones
	void OnCollisionEnter2D(Collision2D c) {

		// Player
		if (c.gameObject.CompareTag("Player")){


			GM.ejecutarSonido(sonidoPickup);

			switch(tipo){
			case TipoPickups.DISPARO:
				GM.minave.aumentaDisparo();
				break;
			case TipoPickups.VELOCIDAD:
				GM.minave.aumentaVelocidad();
				break;
			case TipoPickups.VIDA:
				GM.minave.aumentaVida();
				break;
			}

			Destroy(this.gameObject);
		}
	}


}
