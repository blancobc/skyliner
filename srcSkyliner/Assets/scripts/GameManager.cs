﻿
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;


public enum Estado { INTRO, MENU, JUEGO }

public class GameManager : MonoBehaviour {




	// ----------------------------------------------------
	// Singleton
	// ----------------------------------------------------

	protected GameManager(){}
	private static GameManager _instancia;
	public static GameManager Instancia
	{
		get
		{
			if (_instancia == null)
			{
				_instancia = Object.FindObjectOfType(typeof(GameManager)) as GameManager;

				// si no la encuentro la creo
				if (_instancia == null)
				{
					GameObject go = new GameObject("_gamemanager");
					DontDestroyOnLoad(go);
					_instancia = go.AddComponent<GameManager>();
				}
			}
			return _instancia;
		}
	}




	// ----------------------------------------------------
	// Acciones a realizar al inicializar
	// ----------------------------------------------------
	void Awake(){
		//_instancia = this;
		establecerLimites();
		a = this.gameObject.AddComponent<AudioSource>();
		minave = new Nave();

	}

	// ----------------------------------------------------
	// Atributos
	// ----------------------------------------------------

	private List<GameObject> trozosEscenario;

	public Estado estadoActual {get; set; }

	public Nave minave {get; private set;}


	// ----------------------------------------------------
	// LIMITES
	// ----------------------------------------------------
	public Boundary boundary = new Boundary();
	public void establecerLimites(){
		boundary.xMax = Camera.main.aspect * Camera.main.orthographicSize;
		boundary.xMin = -boundary.xMax;
		boundary.yMax = Camera.main.orthographicSize;
		boundary.yMin = -boundary.yMax;
	}

	// ----------------------------------------------------
	// GESTION DE SONIDOS
	// ----------------------------------------------------
	public AudioSource a;
	public void ejecutarSonido(AudioClip clip){
		a.PlayOneShot(clip);
	}


	// ----------------------------------------------------
	// GESTION DE ESCENAS Y PAUSA
	// ----------------------------------------------------
	public float valorTimeScale;
	public void pausar(){
		if(Time.timeScale > 0){
			valorTimeScale = Time.timeScale;
			Time.timeScale = 0;
		}
		else{
			quitarPausa ();
		}
	}
	public void quitarPausa(){
		if(Time.timeScale == 0) Time.timeScale = valorTimeScale;
	}
	public void irAlMenu(){
		guardarPartida();
		estadoActual = Estado.MENU;
		SceneManager.LoadScene("Menu");
	}
	public void cargaEscena(string escena){
		guardarPartida();
		SceneManager.LoadScene(escena);
	}
	

	// ----------------------------------------------------
	// GESTION DE PARTIDA
	// ----------------------------------------------------
	public int puntos;
	public void guardarPartida(){
		if(PlayerPrefs.GetInt ("puntosMax") < puntos) PlayerPrefs.SetInt("puntosMax", puntos);
	}
	public void cargarPartida(){}
	

		

}