﻿using System;


public class Nave
{

		// ----------------------------------------------------
		// Atributos de la nave
		// ----------------------------------------------------

		// att nave (actuales)
		public int naveVida;
		public float naveVel;
		public float disparoRate;
		public int disparoVelocidad;
		public int disparoDano; 
		public int disparoAncho;

		// att nave (maximos)
		public int naveVidaMax;
		public float naveVelMax;
		public float disparoRateMin;
		public int disparoVelocidadMax;
		public int disparoDanoMax;
		public int disparoAnchoMax;

		// att enemigos
		public int enemigoVida;
		public float enemigoVelocidad;
		public int enemigoDano;
		public float ratioPickups;


		public Nave ()
		{

		// att nave (actuales)
		naveVida = 100; // de 0 a 100
		naveVel = 0.5f; // de 0.5f a 6f
		disparoRate = 0.6f; // de 0.6f a 0.3f
		disparoVelocidad = 5; // de 5 a 15
		disparoDano = 10; // de 10 a 20
		disparoAncho = 1; // de 1 a 5 

		// att nave (maximos)
		naveVidaMax = 100;
		naveVelMax = 6f;
		disparoRateMin = 0.3f;
		disparoVelocidadMax = 15;
		disparoDanoMax = 20;
		disparoAnchoMax = 5;

		// att enemigos
		enemigoVida = 20;
		enemigoVelocidad = 1f;
		enemigoDano = 10;
		ratioPickups = 0.4f;

		}



	// Metodos de gestion de las caracteristicas de la nave

	public void aumentaVida(){
		naveVida += 10;
		if(naveVida > naveVidaMax) naveVida = naveVidaMax;
	}

	public void aumentaVelocidad(){
		naveVel += 0.5f;
		if(naveVel > naveVelMax) naveVel = naveVelMax;
	}

	public void aumentaDisparo(){
		if(disparoRate > disparoRateMin) disparoRate -= 0.1f;
		disparoVelocidad++;
		if(disparoVelocidad > disparoVelocidadMax) disparoVelocidad = disparoVelocidadMax;
		disparoDano += 5;
		if(disparoDano > disparoDanoMax) disparoDano = disparoDanoMax;
		disparoAncho++;
		if(disparoAncho > disparoAnchoMax) disparoAncho = disparoAnchoMax;

	}

	public void reiniciarCaracteristicas(){
		naveVel = 3f;
		disparoRate = 0.6f;
		disparoVelocidad = 5;
		disparoDano = 10;
		disparoAncho = 1;
	}


	public void hacerDano(){
		// pierde las ventajas
		reiniciarCaracteristicas();

		// disminuye vida
		naveVida -= 10;
		if(naveVida<=0){
			// TODO animacion de explotar la nave
			//Invoke("irAlMenu", 2f);
		}
	}


}

