﻿using UnityEngine;
using System.Collections;

public class enemyController : MonoBehaviour {


	// GAME MANAGER
	GameManager GM;
	void Awake () {
		GM = GameManager.Instancia;
	}

	// atributos del enemigo
	Rigidbody2D rb;
	private int enemigoVida;
	private float enemigoVelocidad;
	public AudioClip sonidoRecibirDano;

	// pickup a generar
	public GameObject pickup;



	void Start () {
	
		rb = GetComponent<Rigidbody2D>();

		enemigoVida = GM.minave.enemigoVida;
		enemigoVelocidad = GM.minave.enemigoVelocidad;

	}


	void FixedUpdate () {

		// mov aleatorio
		int x = Random.Range (-1,2);
		Vector2 v = new Vector2(x, -enemigoVelocidad);
		rb.velocity = v;
	
	}

	// matamos los que salen de la pantalla
	void OnBecameInvisible() {
		Destroy( gameObject ); 
	}


	// Colisiones
	void OnCollisionEnter2D(Collision2D c) {

		// Enemigo o disparo
		// Daño y destruir el enemigo o disparo
		if (c.gameObject.CompareTag("Enemy") || c.gameObject.CompareTag("Fire")){
			Destroy(c.gameObject);
			enemigoVida -= 10;
		}

		// Si se queda sin vida elimino el objeto y aleatoriamente genero un pickup
		if(enemigoVida<=0){
			GM.puntos++;
			Destroy(gameObject);
			//a.PlayOneShot(sonidoRecibirDano);
			GM.ejecutarSonido(sonidoRecibirDano);

			if(Random.value < GM.minave.ratioPickups){
				Instantiate(pickup, transform.position, Quaternion.identity);
			}
		}

		// Pickup. Lo destruyo
		if(c.gameObject.CompareTag("Pickup")) Destroy(c.gameObject);

	}


}
