﻿using UnityEngine;
using System.Collections;

public class fireController : MonoBehaviour {
	
	// GAME MANAGER
	GameManager GM;
	void Awake () {
		GM = GameManager.Instancia;
	}

	public float speed;

	void Start () {

		speed = GM.minave.disparoVelocidad;

	}

	void Update () {


		transform.Translate(new Vector2(0, speed * Time.deltaTime));
	

	}

	// matamos los que salen de la pantalla
	void OnBecameInvisible() {
		Destroy( gameObject ); 
	}



}
