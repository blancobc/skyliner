﻿using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	// GAME MANAGER
	GameManager GM;
	void Awake () {
		GM = GameManager.Instancia;
	}


	public Text puntuacion;

	void Start(){

		puntuacion.text = PlayerPrefs.GetInt("puntosmax").ToString();
		Invoke("OpcionJuego",2f);

	}



	// OPCIONES DEL MENU
	
	public void OpcionJuego(){
		GM.estadoActual = Estado.JUEGO;
		GM.cargaEscena("Juego");
	}

	public void OpcionIntro(){
		GM.estadoActual = Estado.INTRO;
		GM.cargaEscena("Intro");
	}

	public void OpcionSalir(){
		Debug.Log("Salimos");
		Application.Quit();
	}

}
