﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {

	// GAME MANAGER
	GameManager GM;
	void Awake () {
		GM = GameManager.Instancia;
	}



	public Slider vidaNave;
	public Text puntosEnemigos;


	void Start () {
	
	}
	
	void Update () {

		vidaNave.value = GM.minave.naveVida;
		puntosEnemigos.text = GM.puntos.ToString();
	
	}
}
